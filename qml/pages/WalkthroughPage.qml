import QtQuick 2.9
import Ubuntu.Components 1.3
import "../components"

// Initial Walkthrough tutorial

Page {
    id: walkthroughPage
    anchors.fill: parent

    header: PageHeader {
        title: ""
        StyleHints {
            dividerColor: "#00000000"
            backgroundColor: "#00000000"
        }
    }

    Walkthrough {
        id: walkthrough
        anchors.fill: parent

        appName: "FluffyChat"

        onFinished: {
            walkthrough.visible = false
            mainLayout.walkthroughFinished = true
            mainLayout.updateInfosFinished = version
            init ()
        }

        model: [


        Component {
            id: slide1

            Rectangle {
                id: slide1Container

                Image {
                    id: smileImage
                    anchors {
                        top: slide1Container.top
                        topMargin: units.gu(4)
                        horizontalCenter: slide1Container.horizontalCenter
                    }
                    height: (parent.height - introductionText.height - bodyText.contentHeight - 4.5*units.gu(4))
                    fillMode: Image.PreserveAspectFit
                    source: Qt.resolvedUrl("../../assets/startup.svg")
                    asynchronous: true
                }

                Label {
                    id: introductionText
                    anchors {
                        bottom: bodyText.top
                        bottomMargin: units.gu(4)
                    }
                    elide: Text.ElideRight
                    fontSize: "x-large"
                    horizontalAlignment: Text.AlignHLeft
                    maximumLineCount: 2
                    text: i18n.tr("Herzlich Willkommen")
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                }

                Label {
                    id: bodyText
                    anchors {
                        bottom: slide1Container.bottom
                        bottomMargin: units.gu(10)
                    }
                    fontSize: "large"
                    height: contentHeight
                    horizontalAlignment: Text.AlignHLeft
                    text: "Wir freuen uns Sie bei famedly, der Zukunft in der Medizin, begrüßen zu dürfen.\n\nAuf den nächsten Seiten möchten wir Ihnen vorstellen, was Sie erwartet."
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                }
            }
        },


        Component {
            id: slide2

            Item {
                id: slide2Container

                Image {
                    id: smileImage
                    anchors {
                        top: slide2Container.top
                        topMargin: units.gu(4)
                        horizontalCenter: slide2Container.horizontalCenter
                    }
                    height: (parent.height - introductionText.height - bodyText.contentHeight - 4.5*units.gu(4))
                    fillMode: Image.PreserveAspectFit
                    source: Qt.resolvedUrl("../../assets/speech_bubble.svg")
                    asynchronous: true
                }

                Label {
                    id: introductionText
                    anchors {
                        bottom: bodyText.top
                        bottomMargin: units.gu(4)
                    }
                    elide: Text.ElideRight
                    fontSize: "x-large"
                    horizontalAlignment: Text.AlignHLeft
                    maximumLineCount: 2
                    text: "Miteinander sprechen"
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                }

                Label {
                    id: bodyText
                    anchors {
                        bottom: slide2Container.bottom
                        bottomMargin: units.gu(10)
                    }
                    fontSize: "large"
                    height: contentHeight
                    horizontalAlignment: Text.AlignHLeft
                    text: "Mit famedly können sie jeden Versorger leicht und sicher erreichen.\n\nKonzentrieren Sie sich auf die Arbeit mit Patienten und überlassen Sie uns den Rest."
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                }
            }
        },


        Component {
            id: slide3

            Item {
                id: slide3Container

                Image {
                    id: smileImage
                    anchors {
                        top: slide3Container.top
                        topMargin: units.gu(4)
                        horizontalCenter: slide3Container.horizontalCenter
                    }
                    height: (parent.height - introductionText.height - bodyText.contentHeight - 4.5*units.gu(4))
                    fillMode: Image.PreserveAspectFit
                    source: Qt.resolvedUrl("../../assets/union.svg")
                    asynchronous: true
                }

                Label {
                    id: introductionText
                    anchors {
                        bottom: bodyText.top
                        bottomMargin: units.gu(4)
                    }
                    elide: Text.ElideRight
                    fontSize: "x-large"
                    horizontalAlignment: Text.AlignHLeft
                    maximumLineCount: 2
                    text: i18n.tr("What's new in version %1?").arg(version)
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                }

                Label {
                    id: bodyText
                    readonly property string template1: i18n.tr("Minor bugfixes and updated translations.")
                    readonly property string template2: i18n.tr("Critical bug fixed.")
                    readonly property string template3: i18n.tr("Security bug fixed.")
                    readonly property string template4: i18n.tr("Updated translations. Thanks to all translators.")
                    anchors {
                        bottom: slide3Container.bottom
                        bottomMargin: units.gu(10)
                    }
                    fontSize: "large"
                    height: contentHeight
                    horizontalAlignment: Text.AlignHLeft
                    text: "Changed default Homeserver to matrix.org"
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                }
            }
        },



        Component {
            id: slide4
            Item {
                id: slide4Container

                Image {
                    id: smileImage
                    anchors {
                        top: slide4Container.top
                        topMargin: units.gu(4)
                        horizontalCenter: slide4Container.horizontalCenter
                    }
                    height: (parent.height - introductionText.height - finalMessage.contentHeight - continueButton.height - 4.5*units.gu(4))
                    visible: height > 0
                    fillMode: Image.PreserveAspectFit
                    source: Qt.resolvedUrl("../../assets/union.svg")
                    asynchronous: true
                }

                Label {
                    id: introductionText
                    anchors {
                        bottom: finalMessage.top
                        bottomMargin: units.gu(4)
                    }
                    elide: Text.ElideRight
                    fontSize: "x-large"
                    horizontalAlignment: Text.AlignHLeft
                    maximumLineCount: 2
                    text: "Verbindungen schaffen"
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                }

                Label {
                    id: finalMessage
                    anchors {
                        bottom: continueButton.top
                        bottomMargin: units.gu(7)
                    }
                    fontSize: "large"
                    horizontalAlignment: Text.AlignHLeft
                    text: "Mit famedly wird es möglich sein, jedes medizinsche Gerät oder Software anzusteuern.\n\nDas bedeutet für sie schnelles und einfaches Arbeiten aus einer Oberfläche."
                    width: units.gu(36)
                    wrapMode: Text.WordWrap
                    linkColor: mainLayout.brightMainColor
                    textFormat: Text.StyledText
                    onLinkActivated: contentHub.openUrlExternally ( link )
                }

                Button {
                    id: continueButton
                    anchors {
                        bottom: slide4Container.bottom
                        bottomMargin: units.gu(3)
                        horizontalCenter: slide4Container.horizontalCenter
                    }
                    color: UbuntuColors.green
                    height: units.gu(5)
                    text: i18n.tr("Continue")
                    width: units.gu(36)

                    onClicked: walkthrough.finished()
                }
            }
        }
        ]
    }
}
