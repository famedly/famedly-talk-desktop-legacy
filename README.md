# Famedly Talk for Desktop

Forked from FluffyChat.

#### How to build

1. Clone this repo:
```
git clone --recurse-submodules https://gitlab.com/famedly/fluffychat
cd fluffychat
```

##### Build Click for Ubuntu Touch

2. Install clickable as described here: https://gitlab.com/clickable/clickable

3. Build with clickable
```
clickable build-libs
clickable click-build
```

##### Build Snap for Desktop

2. Install snapcraft as described here: https://snapcraft.io

3. Build with snapcraft and install
```
snapcraft --debug
snap install [filename].snap --dangerous
```
